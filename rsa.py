#!c:/Python27/python.exe -u
import random


def menu():
    print("****************************************************************")
    print("* RSA algorithm in Python                                      *")
    print("* by Michael Bourque (100258740) *")
    print("* Press 1 for encryption (manually input p and q)              *")
    print("* Press 2 for encryption (auto-generate p and q)               *")
    print("* Press 3 for decryption                                       *")
    print("* Press q for quit                                             *")
    print("****************************************************************")
    choice = raw_input("Enter your choice: ")
    return choice

# this function creates a list of n primes.
# This is used for quick selection of primes
# by other functions


def primes(n):
    if n == 2:
        return [2]
    elif n < 2:
        return []
    s = range(3, n + 1, 2)
    mroot = n ** 0.5
    half = (n + 1) / 2 - 1
    i = 0
    m = 3
    while m <= mroot:
            if s[i]:
                    j = (m * m - 3) / 2
                    s[j] = 0
                    while j < half:
                            s[j] = 0
                            j += m
            i = i + 1
            m = 2 * i + 3
    return [2] + [x for x in s if x]

# this function employs fermat's little theorem to test whether or not a
# number is probably prime or not prime. It is used as a boolean test and
# serves as an alternate method of prime number selection.


def fermat(num, k):
    random.seed()
    rand = random.randint(1, (num - 1))
    for i in range(0, k):
            if ((rand ** (num - 1)) % num) != 1:
                    return False
            else:
                    rand = random.randint(1, (num - 1))
    return True

# this function returns the GCD of two integers


def gcd(a, b):
    while b:
            a, b = b, a % b
    return a

# this function uses the extended euclidian algorithm to solve the equation:
# ax + by = gcd (a,b) where a and b are known (passed by parameter) and one
# of x or y is returned


def egcd(p, q):
    if p == 0:
            return (q, 0, 1)
    else:
            g, y, x = egcd(q % p, p)
            return (g, x - (q // p) * y, y)

# this function is used in conjuntion with the egcd function to find the
# multiplicative inverse of e.


def inverse(e, phin):
    g, x, y = egcd(e, phin)
    if g != 1:
            raise Exception('Inverse does not exist!')
    else:
            return x % phin

# this function is used to display the contents of an array or list


def displaylist(list_or_iterator):
    return "".join(str(x) for x in list_or_iterator)

# this function is used for query control


def confirm(prompt=None, resp=False):
    if prompt is None:
        prompt = "Confirm"
    if resp:
        prompt = '%s [%s]|%s: ' % (prompt, 'y', 'n')
    else:
        prompt = '%s [%s]|%s: ' % (prompt, 'n', 'y')
    while True:
        ans = raw_input(prompt)
        if not ans:
            return resp
        if ans not in ['y', 'Y', 'n', 'N']:
            print("Please enter y or n")
            continue
        if ans == 'y' or ans == 'Y':
            return True
        if ans == 'n' or ans == 'N':
            return False

# this function invokes a low bit RSA encryption scheme using either
# specified p and q prime numbers or generated values.


def encryptRSA(p, q):
    if p == 0 and q == 0:
            pqvals = False
            while 1:
                    p = long(raw_input("Enter p: "))
                    q = long(raw_input("Enter q: "))
                    if p * q > 256 and p != q:
                            if fermat(p, 1000) is True:
                                    if fermat(q, 1000) is True:
                                            break
                                    else:
                                            print("q is not prime!")
                            else:
                                    print("p is not prime!")
                    elif p == q:
                            print("q must be different from p!")
                    else:
                            print("This RSA implementation relies on ASCII encoding values (0-255)")
                            print("This means that 'n' must be at least 16bit (256) to encrypt properly")
                            print("Please choose larger values for p and q")
                    smalln = confirm(prompt='Would you like me to display a list of prime numbers to choose from?', resp=True)
                    if smalln is True:
                            print(primes(100))
    n = p * q
    phin = (p - 1) * (q - 1)
    e = 3
    while 1:
            if gcd(e, phin) == 1 and 1 < e and e < phin:
                    d = inverse(e, phin)
                    ed = e * d
                    if gcd(ed, phin) == 1 and 1 < d and d < phin:
                            break
            else:
                    e = e + 1
    print("")
    print("Key Generation complete...")
    print("| p =", p, "| q =", q, "| n =", n, "| phi(n) =", phin, "| e =", e, "| d =", d)
    print("................................................................")
    msg = raw_input("Enter message to encrypt (M): ")
    enc_text = []
    for i in msg:
            M = ord(i)
            C = M ** e % n
            if hex(C)[-1] == "L":
                    enc_text.append(hex(C)[:-1])
            else:
                    enc_text.append(hex(C))
    enc_output = displaylist(enc_text)
    try:
            with open('RSAout.txt', 'w') as file:
                    file.write(enc_output)
    except IOError as e:
        print("Unable to save file RSAout.txt. Moving on.")
        print("")
        print("Encrypted message is:")
        print("---------------------------------------------------------------")
        print(enc_output)
        print("---------------------------------------------------------------")
        print(" Public key {n,e} is: {", n, ",", e, "}")
        print("Private key {n,d} is: {", n, ",", d, "}")
        print("")
        main()

# this function employs a low-bit RSA decrytpion scheme using a provided
# Private Key and Ciphertext.


def decryptRSA():
    try:
            with open('RSAout.txt', 'r') as file:
                pass
            useoutput = confirm(prompt='RSAout.txt found! Should I use its contents as encrypted message (M)?', resp=True)
    except IOError as e:
            print("No saved RSAout.txt found. Moving on.")
    print("Private keys ( n , d )")
    n = long(raw_input("Enter n: "))
    d = long(raw_input("Enter d: "))
    if useoutput is True:
            with open('RSAout.txt') as file:
                    line = file.readline()
    else:
            line = raw_input("Enter message to decrypt (C): ")
    dec_text = []
    msg = line.split('0x')
    for i in range(len(msg) - 1):
            M = long(str(msg[i + 1]), 16)
            C = M ** d % n
            dec_text.append(chr(C))
    print("Decrypted message is:")
    print("---------------------------------------------------------------")
    print(displaylist(dec_text))
    print("---------------------------------------------------------------")
    print("")
    main()

# this function controls the program and the menu


def main():
    choice = menu()
    random.seed()
    if choice == "1":
            encryptRSA(0, 0)
    elif choice == "2":
            plist = primes(100)
            while 1:
                    p = random.randint(0, len(plist) - 1)
                    q = random.randint(0, len(plist) - 1)
                    for index, item in enumerate(plist):
                            if p == index:
                                    p = item
                            if q == index:
                                    q = item
                    if (p - 1) * (q - 1) > 256 and p != q:
                            break
            encryptRSA(p, q)
    elif choice == "3":
            decryptRSA()
    else:
            print("Thank you for using this program!")

if __name__ == "__main__":
    main()
